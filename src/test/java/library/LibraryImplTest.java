package library;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LibraryImplTest {

    @Test
    public void addNewBook() {
        LibraryImpl library = new LibraryImpl();
        library.addNewBook(new Book("1", "Zxc", "Me"));

        assertTrue(library.addNewBook(new Book("2", "A", "B")));
        assertFalse(library.addNewBook(new Book("1", "C", "D")));
    }

    @Test
    public void deleteBook() {
        LibraryImpl library = new LibraryImpl();
        library.addNewBook(new Book("1", "Zxc", "Me"));
        library.addNewBook(new Book("2", "Hello", "World"));

        assertFalse(library.listBooksByName("Zxc").isEmpty());
        assertFalse(library.listBooksByName("Hello").isEmpty());
        library.deleteBook("1");
        assertTrue(library.listBooksByName("Zxc").isEmpty());
        library.deleteBook("2");
        assertTrue(library.listBooksByName("Hello").isEmpty());
    }

    @Test
    public void listBooksByName() {
        LibraryImpl library = new LibraryImpl();
        library.addNewBook(new Book("1", "Zxc", "Me"));
        library.addNewBook(new Book("2", "Asd", "Him"));
        library.addNewBook(new Book("3", "Test", "Her"));
        library.addNewBook(new Book("4", "Hello", "They"));
        library.addNewBook(new Book("5", "World", "Our"));
        library.addNewBook(new Book("6", "Test", "Me"));
        assertEquals(2, library.listBooksByName("Test").size());
    }

    @Test
    public void listBooksByAuthor() {
        LibraryImpl library = new LibraryImpl();
        library.addNewBook(new Book("1", "Zxc", "Me"));
        library.addNewBook(new Book("2", "Asd", "Him"));
        library.addNewBook(new Book("3", "Test", "Her"));
        library.addNewBook(new Book("4", "Hello", "They"));
        library.addNewBook(new Book("5", "World", "Our"));
        library.addNewBook(new Book("6", "Setup", "Me"));
        assertEquals(2, library.listBooksByAuthor("Me").size());
    }
}
