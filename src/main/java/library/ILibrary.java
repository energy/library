package library;

import java.util.List;

public interface ILibrary {
    // ensure that id is unique, otherwise return false
    boolean addNewBook(Book book);

    // delete book with the specified id
    void deleteBook(String id);

    // return 10 book names containing the specified string
    // if there are several books with the same name, append author's name to
    // book's name
    List<String> listBooksByName(String searchString);

    // return 10 book names whose author contains specified string
    // order books by author
    List<String> listBooksByAuthor(String searchString);

}
