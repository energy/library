package library;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class LibraryImpl implements ILibrary {

    private Map<String, Book> storage = new ConcurrentHashMap<>();

    // ensure that id is unique, otherwise return false
    @Override
    public synchronized boolean addNewBook(Book book) {
        if (storage.containsKey(book.getId())) {
            return false;
        }
        storage.put(book.getId(), book);
        return true;
    }

    // delete book with the specified id
    @Override
    public void deleteBook(String id) {
        storage.remove(id);
    }

    // return 10 book names containing the specified string
    // if there are several books with the same name, append author's name to
    // book's name
    @Override
    public List<String> listBooksByName(final String searchString) {
        List<Book> books = storage.values()
                .stream()
                .filter(b -> b.getName().contains(searchString))
                .limit(10L)
                .collect(toList());

        return books.stream().map(book -> {
            String name = book.getName();
            if (books.stream().filter(b -> b.getName().equals(book.getName())).count() > 1) {
                name += ", " + book.getAuthor();
            }
            return name;
        }).collect(toList());
    }

    // return 10 book names whose author contains specified string
    // order books by author
    @Override
    public List<String> listBooksByAuthor(final String searchString) {
        return storage.values()
                .stream()
                .filter(b -> b.getAuthor().contains(searchString))
                .limit(10L)
                .sorted(Comparator.comparing(Book::getAuthor))
                .map(Book::getName)
                .collect(toList());
    }
}
